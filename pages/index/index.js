// index.js
// 获取应用实例
const app = getApp()

Page({
  data: {
    phoneValue: '',
    faceValue: [{
      title:'30元',infos:'售价30.00元',value:'30.00'
    },{
      title:'50元',infos:'售价50.00元',value:'50.00'
    },{
      title:'100元',infos:'售价100.00元',value:'100.00'
    },{
      title:'200元',infos:'售价200.00元',value:'200.00'
    },{
      title:'300元',infos:'售价300.00元',value:'300.00'
    },{
      title:'500元',infos:'售价500.00元',value:'500.00'
    },],
    moneyValue: '',
    is_sure: false,
    displayPhone: '',
    focus: false
  },

  onLoad() {

  },

  openKeyboard() {
    this.setData({
      focus: true
    })
  },
  phoneValue(e) {
    let phoneNum = e.detail.value
    let phoneReg = /^[9][0-9]{9}$/
    let sure = false
    let displayNumber = '(' + phoneNum.slice(0,3) + ')'
    if (phoneNum.length > 3) {
      displayNumber += phoneNum.slice(3, 6)
      if (phoneNum.length > 6) {
        displayNumber += '-' + phoneNum.slice(6,8)
        if (phoneNum.length > 8) {
          displayNumber += '-' + phoneNum.slice(8,10)
        }
      }
    }
    if (phoneNum !== '' && phoneReg.test(phoneNum)) {
      sure = true
    } else {
      sure = false
    }
    this.setData({
      is_sure: sure,
      phoneValue: phoneNum,
      displayPhone: displayNumber
    })
  },
  clearPhone() {
    this.setData({
      is_sure: false,
      phoneValue:''
    })
  },
  recharge(e) {
    let _this = this
    let item = e.currentTarget.dataset.item
    let phoneValue = this.data.phoneValue
    if (this.is_sure) {
    //   wx.showToast({
    //     title: '请填写正确的手机号码',
    //     icon:'none'
    //   })
    // }else{
      if (item) {
        wx.showToast({
          title: '充值'+ item.value,
          success(){
            wx.reLaunch({
              url: '/pages/jump/index',
            })
          }
        })
      } else {
        let money = parseInt(this.data.moneyValue)
        if(money > 0) {
          wx.showToast({
            title: '充值'+ money.toFixed(2),
            success() {
              _this.setData({moneyValue:''})
            }
          })
        }else{
          wx.showToast({
            title:'请填写金额',
            icon:'none'
          })
        }
      }
    }
  },
  bindMoneyValue(e) {
    this.setData({moneyValue:e.detail.value})
  }
})
