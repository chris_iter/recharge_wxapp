// pages/record/index.js
Page({

  data: {
    recordData: ['','','','',]
  },

  delete() {
    wx.showModal({
      title: '提示',
      content: '确认删除？',
      success(res) {
        if(res.confirm){
          wx.showToast({
            title: '已删除',
          })
        }
      }
    })
  },

  onLoad: function (options) {

  },

  onShow: function () {

  },

  onPullDownRefresh: function () {

  },

  onReachBottom: function () {

  },

  onShareAppMessage: function () {

  }
})