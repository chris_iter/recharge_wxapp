// pages/jump/index.js
Page({

  data: {
    is_success: true
  },

  doJump() {
    wx.switchTab({
      url: '/pages/record/index',
    })
  },

  onLoad: function (options) {

  },

  onShow: function () {

  },

  onPullDownRefresh: function () {

  },

  onReachBottom: function () {

  },

  onShareAppMessage: function () {

  }
})